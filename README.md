Use Docker to run the app and make sure you on good connection if not maybe you will get error, if you experience the error try to re-clone the repo.

Before we begin make sure no docker container is runing, if it's try to stop and remove them, so let's begin with:
1. Navigate to 'assessment/frontend' folder and then run 'npm install',
2. Navigate to 'assessment/' folder and then run 'docker-compose up'

Incase code is not running, try to run frontend outside the docker by commenting 'frontend' configuration bracket on the docker-compose.yml ( line number 38 - 46) and then:
1. Navigate to 'assessment/frontend' folder and then run 'npm start'
2. Navigate to 'assessment/' folder and then run 'docker-compose'

If everything steps above is not work please kindly to try download the code frome this link below:
https://drive.google.com/open?id=174mFzPmHtheInhwWYotJL5yjvvvn363p
and then run 'docker-compose up' from 'assessment' folder.

localhost:3000 to access the application

Note: bepatience while installing also if you have issues or problem don't hesitate to contact me.

Thanks.