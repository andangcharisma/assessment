import React from 'react';

const navbarContext = React.createContext({
  title: ''
});

export default navbarContext;