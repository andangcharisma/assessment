import React from 'react';
import {Link} from "react-router-dom";

import style from './ProductSmall.module.scss';

const productSmall = props => (
  <div className={style.Wrapper}>
    <Link to={props.linkUrl}>
      <img alt="" src={props.imageUrl}/>
    </Link>
    <div className={style.BottomWrapper}>
      <div className={style.Info}>
        <div className={style.Title}>{props.title}</div>
        <div className={style.Price}>{props.price}</div>
      </div>
      <div className={style.Favourite}>
        <i className="fa fa-heart-o" aria-hidden="true"></i>
      </div>
    </div>
  </div>
);

export default productSmall;