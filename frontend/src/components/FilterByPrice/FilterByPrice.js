import React from 'react';
import {Link} from "react-router-dom";

import style from './FilterByPrice.module.scss';

const filterByPrice = props => (
  <div className={style.Wrapper}>
    <Link to={props.filterUrl}>
      <div className={style.Link}>
        <div className={style.Price}>{props.priceRange}</div>
      </div>
    </Link>
  </div>
);

export default filterByPrice;