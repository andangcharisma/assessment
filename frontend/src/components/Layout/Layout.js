import React, {useContext} from 'react';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  Row,
  Col
} from 'reactstrap';
import {Link} from 'react-router-dom';

import NavbarContext from '../../context/navbar-context';
import style from './Layout.module.scss';


const Layout = props => {
  const navbarInfo = useContext(NavbarContext);
  let title = 'Dress';
  if (navbarInfo.title !== '') {
    title = navbarInfo.title;
  }
  return (
    <div className={style.LayoutBackground}>
      <Navbar color="light" light expand="md" fixed="top">
        <div className={style.LayoutContainer}>
          <Row>
            <Col xs="9">
              <Button color="link" className={style.Back}>
                <Link to="/">
                  <i className="fa fa-arrow-left" aria-hidden="true"></i>
                </Link>
              </Button>
              <NavbarBrand className={style.Title}>{title}</NavbarBrand>
            </Col>
            <Col xs="3">
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink href="/">
                    <i className="fa fa-search" aria-hidden="true"></i>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/">
                    <i className="fa fa-heart-o" aria-hidden="true"></i>
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/">
                    <i className="fa fa-shopping-bag" aria-hidden="true"></i>
                  </NavLink>
                </NavItem>
              </Nav>
            </Col>
          </Row>
        </div>
      </Navbar>

      <main className={style.LayoutContainer}>
        <div className={style.Spacer}></div>
        {props.children}
      </main>
    </div>
  )
};

export default Layout;