import React from "react";
import {Button, Card, CardBody, CardHeader, Col, Row} from "reactstrap";

const CommonCard = props => {
  const checkHeader = () => {
    if (props.pageNavText) {
      return (
        <Row>
          <Col xs="7">{props.title}</Col>
          <Col xs="5" className="text-right">
            <Button color="link">{props.pageNavText}</Button>
          </Col>
        </Row>
      );
    } else {
      return (
        <Row>
          <Col xs="12">{props.title}</Col>
        </Row>
      );
    }
  };

  return (<Row>
      <Col xs="12">
        <Card>
          <CardHeader tag="h5">
            {checkHeader()}
          </CardHeader>
          <CardBody>
            {props.children}
          </CardBody>
        </Card>
      </Col>
    </Row>
  );
};

export default CommonCard;