import React, {useState} from 'react';
import style from "./DetailProduct.module.scss";
import {Card, CardBody, CardHeader, Col, Row, Button, Modal, ModalHeader, ModalBody} from "reactstrap";

const DetailProduct = props => {
  const [detailSize, setDetailSize] = useState('');
  const [detailSizeIcon, setDetailSizeIcon] = useState(<i className="fa fa-chevron-down" aria-hidden="true"></i>);
  const toggleDetail = () => {
    if (detailSize === '') {
      setDetailSize(
        <CardBody>
          <div className={style.Wrapper}>
            <div className={style.Box}>
              <div className={style.SubTitle}>DETAIL</div>
              <div className={style.Text}>Kancing depan</div>
            </div>
            <div className={style.AddLine}></div>

            <div className={style.Box}>
              <div className={style.SubTitle}>Panduan Ukuran</div>
              <div className={style.Text}>Warna:  Magenta / Teal / Tosca (kiri ke kanan) </div>
            </div>
            <div className={style.AddLine}></div>

            <div className={style.Box}>
              <div className={style.SubTitle}>Produk Bisa Dicoba dan Dikembalikan</div>
              <div className={style.Text}>Ya</div>
            </div>
          </div>
        </CardBody>
      );
      setDetailSizeIcon(<i className="fa fa-chevron-up" aria-hidden="true"></i>)
    } else {
      setDetailSize('');
      setDetailSizeIcon(<i className="fa fa-chevron-down" aria-hidden="true"></i>)
    }
  };

  const [isModalOpen, setModal] = useState(false);
  const toggle = () => setModal(!isModalOpen);

  return (
    <Row>
      <Col xs="12">
        <Card>
          <CardHeader className={style.Cardheader}>
            <Row>
              <Col xs="4">
                <div className={style.Section}>Coba Dulu <br/> Baru Bayar</div>
              </Col>
              <Col xs="4">
                <div className={style.Section}>COD <br/> Se-Indonesia</div>
              </Col>
              <Col xs="4">
                <div className={style.Section}>Garansi <br/> 30 Hari</div>
              </Col>
            </Row>
          </CardHeader>
          <CardBody>
            <div className={style.Wrapper}>
              <div className={style.ImgWrapper} onClick={toggle}>
                <img alt="" src={props.imageUrl}/>
                <span className={style.Notice}>Produk Terlaris</span>
              </div>
              <div className={style.Box}>
                <div className={style.Title}>{props.title}</div>
                <div className={style.Price}>{props.price}</div>
                <div className={style.TryFirst}><i className="fa fa-shopping-bag" aria-hidden="true"></i> Bisa Coba Dulu</div>
              </div>
              <div className={style.AddLine}></div>

              <div className={style.Box}>
                <div className={style.SubTitle}>Bahan Utama</div>
                <div className={style.Text}>Katun</div>
              </div>
              <div className={style.AddLine}></div>

              <div className={style.Box}>
                <div className={style.Text}>
                  Warna: Magenta, Teal, Tosca
                  <br/>
                  Ukuran: XXL, XXXL, XXXXXL
                </div>
                <div className={style.SizeGuide} onClick={toggleDetail}>
                  <i className="fa fa-calendar-check-o" aria-hidden="true"></i> Panduan Ukuran
                </div>
              </div>
              <div className={style.AddLine}></div>

              <div className={style.Box}>
                <Row>
                  <Col xs="4">
                    <Button outline color="secondary" block>
                      <i className="fa fa-heart-o" aria-hidden="true"></i> SIMPAN
                    </Button>
                  </Col>
                  <Col xs="8">
                    <Button color="primary" block>
                      BELI SEKARANG
                    </Button>
                  </Col>
                </Row>
              </div>
            </div>
          </CardBody>
        </Card>
      </Col>

      <Modal isOpen={isModalOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}></ModalHeader>
        <ModalBody><img alt="" src={props.imageUrl}/></ModalBody>
      </Modal>

      <Modal isOpen={isModalOpen} toggle={toggle}>
        <ModalHeader toggle={toggle}></ModalHeader>
        <ModalBody><img alt="" src={props.imageUrl}/></ModalBody>
      </Modal>

      <Col xs="12">
        <Card>
          <CardHeader tag="h5">
            <Row>
              <Col xs="7">Detail & Ukuran</Col>
              <Col xs="5" className="text-right">
                <Button color="link" onClick={toggleDetail}>
                  {detailSizeIcon}
                </Button>
              </Col>
            </Row>
          </CardHeader>
          {detailSize}
        </Card>
      </Col>
    </Row>
  );
};

export default DetailProduct;