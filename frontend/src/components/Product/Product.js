import React from 'react';
import {Link} from 'react-router-dom';

import style from './Product.module.scss';
import {Button, Col, Row} from "reactstrap";

const product = props => {
  // console.log(props)
  return (
    <div className={style.Wrapper}>
      <Link to={props.linkUrl}>
        <img alt="" src={props.imageUrl}/>
        <span className={style.Notice}>Produk Terlaris</span>
      </Link>
      <div className={style.Box}>
        <div className={style.Left}>
          <div className={style.Title}>{props.title}</div>
          <div className={style.Size}>L, M, S, XL</div>
          <div className={style.Price}>{props.price}</div>
        </div>
        <div className={style.Right}>
          <Row>
            <Col xs="4">
              <Button outline color="link">
                <i className="fa fa-heart-o" aria-hidden="true"></i>
              </Button>
            </Col>
            <Col xs="8">
              <Button color="primary" block>
                BELI
              </Button>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default product;