import React from 'react';
import {Link} from "react-router-dom";

import style from './ProductCategory.module.scss';

const productCategory = props => (
  <div className={style.Wrapper}>
    <Link to={props.categoryUrl}>
      <img alt="" src={props.imageUrl}/>
      <div className={style.Category}>{props.categoryName}</div>
    </Link>
  </div>
);

export default productCategory;