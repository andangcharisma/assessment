import React, {useEffect, useState} from 'react';
import axios from "axios";

import Layout from '../../components/Layout/Layout';
import DetailProduct from "../../components/DetailProduct/DetailProduct";
import ProductList from "../ProductList/ProductList";
import NavbarContext from "../../context/navbar-context";

const Product = props => {
  const [product, setProduct] = useState([]);
  useEffect(() => {
    const fetchData = () => {
      axios.get(`http://localhost:1337/products?_id_eq=${props.match.params.id}`).then(response => {
        setProduct(response.data[0]);
      })
    };
    fetchData()
  }, [props.match.params.id]);

  return (
    <NavbarContext.Provider value={{title: product.name}}>
    <Layout>
      <DetailProduct
        title={product.name}
        price={product.price}
        imageUrl={product.image}
        linkUrl="/product"/>

      <ProductList
        title="Rekomendasi Produk Lainnya"/>
    </Layout>
    </NavbarContext.Provider>
  );
};

export default Product;