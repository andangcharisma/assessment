import React from 'react';

import ProductList from "../ProductList/ProductList";
import Layout from '../../components/Layout/Layout';

const ProductAll = props => {

  return (
    <Layout>
      <ProductList
        filter={props.match.params.query}
        title="Rekomendasi Produk"/>
    </Layout>
  );
};

export default ProductAll;