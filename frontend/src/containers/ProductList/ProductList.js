import React from 'react'
import { InfiniteScroll } from 'react-simple-infinite-scroll'

import CommonCard from "../../components/CommonCard/CommonCard";
import Product from '../../components/Product/Product';

class ProductList extends React.Component {
  state = {
    items: [],
    isLoading: true,
    cursor: 0
  };

  componentDidMount() {
    this.loadMore()
  }

  loadMore = () => {
    this.setState({ isLoading: true, error: undefined });
    const filterArr = [`_start=${this.state.cursor}&_limit=1`];
    if (this.props.filter) filterArr.push(this.props.filter);
    fetch(`http://localhost:1337/products?${filterArr.join('&')}`)
      .then(res => res.json())
      .then(
        res => {
          this.setState(state => ({
            items: [...state.items, ...res],
            cursor: state.cursor + 1,
            isLoading: false
          }));
        },
        error => {
          this.setState({ isLoading: false, error })
        }
      )
  };

  render() {
    return (
      <CommonCard title={this.props.title}>
        <InfiniteScroll
          throttle={100}
          threshold={300}
          isLoading={this.state.isLoading}
          hasMore={!!this.state.cursor}
          onLoadMore={this.loadMore}>

          {this.state.items.length > 0
            ? this.state.items.map(item => (
              <Product
                key={item.id}
                title={item.name}
                price={item.price}
                imageUrl={item.image}
                linkUrl={`/product/${item.id}`}/>
            ))
            : null}
        </InfiniteScroll>
        {this.state.isLoading && (
          <span>loading...</span>
        )}
      </CommonCard>
    )
  }
}

export  default ProductList;