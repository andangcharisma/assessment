import React, {useEffect, useState} from 'react';
import axios from 'axios';

import CommonCard from '../../../components/CommonCard/CommonCard';
import ProductSmall from '../../../components/ProductSmall/ProductSmall';

const NewProduct = () => {
  const [newProductsData, setNewProductsData] = useState([]);
  useEffect(() => {
    const fetchData = () => {
      axios.get('http://localhost:1337/products?_sort=createdAt:ASC&_limit=2').then(response => {
        setNewProductsData(response.data);
      })
    };
    fetchData()
  }, []);

  const products = newProductsData.map((product, index) => {
    return <ProductSmall
      key={index}
      title={product.name}
      price={product.price}
      imageUrl={product.image}
      linkUrl={`/product/${product.id}`}/>
  });

  return (
    <CommonCard title="Terbaru" pageNavText="Lihat Semua >">
      {products}
    </CommonCard>
  )
};

export default NewProduct;