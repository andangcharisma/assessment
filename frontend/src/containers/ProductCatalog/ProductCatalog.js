import React from 'react';
import {CardImg} from 'reactstrap';

import Layout from '../../components/Layout/Layout';
import CommonCard from '../../components/CommonCard/CommonCard';
import NewProduct from './NewProduct/NewProduct';
import FilterByPrice from '../../components/FilterByPrice/FilterByPrice';
import ProductList from '../ProductList/ProductList';
import CategoryMenu from '../ProductCatalog/CategoryMenu/CategoryMenu';

const productCatalog = () => (
  <Layout>
    <NewProduct/>
    <CategoryMenu/>

    <CommonCard title="Berdasarkan Harga">
      <FilterByPrice priceRange="Di bawah Rp 150.000" filterUrl="products/price_lt=150000"/>
      <FilterByPrice priceRange="150.000 - 250.000" filterUrl="products/price_gt=150000&price_lt=250000"/>
    </CommonCard>

    <CommonCard title="Editorial">
      <a href="example.com" send_referrer="false" target="_blank">
        <CardImg
          top width="100%"
          src="https://imager-next.freetls.fastly.net/images/resized/480/assets-category-banner/250619_Sub-Category_DRESS.jpg"
          alt="Card image cap"/>
      </a>
    </CommonCard>

    <ProductList
      title="Rekomendasi Produk"/>
  </Layout>
);

export default productCatalog;