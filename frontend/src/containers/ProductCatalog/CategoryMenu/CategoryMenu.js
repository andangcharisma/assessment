import React, {useState, useEffect} from 'react';
import axios from 'axios';

import CommonCard from '../../../components/CommonCard/CommonCard';
import ProductCategory from '../../../components/ProductCategory/ProductCategory';

const CategoryMenu = () => {
  const [categoryData, setCategoryData] = useState([]);
  useEffect(() => {
    const fetchData = () => {
      axios.get('http://localhost:1337/productcategories?_sort=category:ASC&_limit=3').then(response => {
        setCategoryData(response.data);
      })
    };
    fetchData()
  }, []);

  const categories = categoryData.map((category, index) => {
    return <ProductCategory
      key={index}
      categoryUrl={`products/category_eq=${category.category}`}
      categoryName={category.category}
      imageUrl={category.image}/>
  });

  return (
    <CommonCard title="Kategori">
      {categories}
    </CommonCard>
  );
};

export default CategoryMenu;