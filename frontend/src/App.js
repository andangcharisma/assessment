import React from 'react';
import {Route, Switch} from "react-router-dom";

import ProductCatalog from './containers/ProductCatalog/ProductCatalog';
import Product from './containers/Product/Product';
import ProductAll from "./containers/ProductAll/ProductAll";

const app = () => (
  <div>
    <div>
      <Switch>
        <Route path="/product/:id" component={Product}/>
        <Route path="/products/:query" component={ProductAll}/>
        <Route path="/" exact component={ProductCatalog}/>
      </Switch>
    </div>
  </div>
);

export default app;
