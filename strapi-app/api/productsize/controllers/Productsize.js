'use strict';

/**
 * Productsize.js controller
 *
 * @description: A set of functions called "actions" for managing `Productsize`.
 */

module.exports = {

  /**
   * Retrieve productsize records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.productsize.search(ctx.query);
    } else {
      return strapi.services.productsize.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a productsize record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.productsize.fetch(ctx.params);
  },

  /**
   * Count productsize records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.productsize.count(ctx.query);
  },

  /**
   * Create a/an productsize record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.productsize.add(ctx.request.body);
  },

  /**
   * Update a/an productsize record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.productsize.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an productsize record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.productsize.remove(ctx.params);
  }
};
